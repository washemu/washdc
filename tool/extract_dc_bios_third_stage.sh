#!/bin/bash

################################################################################
#
#
#   WashingtonDC Dreamcast Emulator
#   Copyright (C) 2023 snickerbockers
#   snickerbockers@washemu.org
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#
################################################################################

################################################################################
#
# simple tool for extracting the third-stage bootloader out of the SEGA
# Dreamcast firmware (commonly referred to as "dc_bios.bin").  The version
# of the firmware that this is designed for has the following checksums:
#
# md5sum:    e10c53c2f8b90bab96ead2d368858623
# sha256sum: 88d6a666495ad14ab5988d8cb730533cfc94ec2cfd53a7eeda14642ab0d4abf9
#
# The third-stage bootloader is loaded from firmware into memory by the
# second-stage bootloader.  Unlike the second stage, the third stage is not
# stored backwards.  it has a total length of 1fff00 bytes.
#
# the base address of the third-stage bootloader is 8c000120.  It is stored in
# firmware at offset 0x100.
#
################################################################################

in_path=$1
out_path=$2

if test "$#" -ne 2 ; then
    echo "usage: $0 <dc_bios.bin path> <output file path>"
    exit 1
fi

dd if=$in_path of=$out_path bs=4 count=524224 skip=64 > /dev/null 2>&1 || exit 1

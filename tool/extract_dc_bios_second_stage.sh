#!/bin/bash

################################################################################
#
#
#   WashingtonDC Dreamcast Emulator
#   Copyright (C) 2023 snickerbockers
#   snickerbockers@washemu.org
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#
################################################################################

################################################################################
#
# simple tool for extracting the second-stage bootloader out of the SEGA
# Dreamcast firmware (commonly referred to as "dc_bios.bin").  The version
# of the firmware that this is designed for has the following checksums:
#
# md5sum:    e10c53c2f8b90bab96ead2d368858623
# sha256sum: 88d6a666495ad14ab5988d8cb730533cfc94ec2cfd53a7eeda14642ab0d4abf9
#
# the second-stage bootloader is a small 32-byte program stored in dc_bios.bin.
# the way the dreamcast boots is that the firmware, mapped into a0000000,
# initializes several low-level bus-state registers, then loads the second-stage
# bootloader into 8c0000e0.  Then it jumps to this address, and the second-stage
# bootloader immediately loads more code from RAM.
#
# the base address of the second-stage bootloader is 8c0000e0.  It is stored in
# firmware at offset e0, although the instructions are all stored in reverse (ie
# the last one is at the beginning and the first one is at the end).  This program
# will correct the order to match the order in-memory as it extracts.
#
################################################################################

in_path=$1
out_path=$2

if test "$#" -ne 2 ; then
    echo "usage: $0 <dc_bios.bin path> <output file path>"
    exit 1
fi

# create output file, we will overwrite this later
dd if=/dev/zero of=$out_path bs=2 count=1 >/dev/null 2>&1 || exit 1

iter=0
while test $iter -ne 16 ; do
    dd if=$in_path of=$out_path bs=2 count=1 skip=$(expr $iter + 112) seek=$(expr 15 - $iter)  conv=notrunc
    iter=$(expr $iter + 1)
done
